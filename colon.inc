%define NEXT 0 ; define initial pointer to be 0
%macro colon 2
    %2:
    dq NEXT ; put pointer in memory
    db %1, 0 ; put key in memory (NUL terminated)
    %define NEXT %2 ; redefine pointer so it points to the previous entry
%endmacro
