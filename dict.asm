%include "lib.inc"
%define KEY_SHIFT 8

global find_word
global get_value

section .text

; searches for an entry with specified key in a dictionary
; rdi - pointer to the dictionary
; rsi - pointer to a string containg a key
; returns a pointer to the entry or a 0 if one wasn't found
find_word:
    .loop:
    test rdi, rdi ; check if pointer is empty, if it is, goto fail
    je .fail
    push rdi ; save calee-saved args
    push rsi
    add rdi, KEY_SHIFT ; right now rsi is pointing to the address of the next entry in the dictionary
    ; add 8 bytes so it points to the key, and not the next entry pointer
    call string_equals ; check if the keys are the same
    pop rsi ; retrieve callee-saved args
    pop rdi
    test rax, rax ; if the keys are the same, goto success
    jne .success
    mov rdi, [rdi] ; move onto the next entry
    jmp .loop ; continue the loop
    .success:
    mov rax, rdi ; put pointer to the dict entry into rax
    ret
    .fail:
    xor rax, rax ; put 0 into rax (entry wasn't found)
    ret
