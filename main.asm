%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define STDOUT 1
%define STDERR 2
%define ENTRY_SHIFT 8
%define MAX_SIZE 255
%define ERROR 1

global _start

section .bss

text_buffer: resb MAX_SIZE

section .rodata

not_found_msg: db "Entry with this key wasn't found", 0

section .text

_start:
    mov rdx, MAX_SIZE
    mov rsi, text_buffer
    call read_input ; read text into buffer
    mov r8, rax ; save key length to r8
    mov rdi, first
    mov rsi, text_buffer
    push r8
    call find_word ; find an entry where the key is equal to the value in buffer
    pop r8
    test rax, rax ; if no such entry was found, goto not_found
    je .not_found
    mov rdi, rax ; put entry pointer into rdi
    add rdi, ENTRY_SHIFT ; move rdi by 8 bytes so it points to the key
    add rdi, r8 ; move rdi by the length of the key so it points to the value
    inc rdi ; + 1 to count in the key string termination
    mov r9, STDOUT ; set output to stdout
    call print_string ; print the value
    call print_newline
    xor rdi, rdi ; move error code 0 to rdi
    call exit
    .not_found:
    mov rdi, not_found_msg ; move pointer to the error msg into rdi
    mov r9, STDERR ; set output to stderr
    call print_string ; print the error
    call print_newline
    mov rdi, ERROR ; move error code 1 to rdi
    call exit
