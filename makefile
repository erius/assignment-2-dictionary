ASM = nasm
ASMFLAGS = -f elf64 -g -o
LD = ld

%.o: %.inc
	$(ASM) $(ASMFLAGS) $@ $<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) $@ $<

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) $@ $<

main.o: main.asm words.o lib.o dict.o
	$(ASM) $(ASMFLAGS) $@ $<

program: main.o words.o dict.o lib.o
	$(LD) -o $@ $^

.PHONY: clean all

clean:
	$(RM) *.o
	$(RM) program

all: program
